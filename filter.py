import sqlite3
import re
import string
from nltk.tokenize import word_tokenize, sent_tokenize
from nltk.corpus import names, stopwords

con = sqlite3.connect('./data.sqlite3')
data = {'id': [], 'abstract_primary': [], 'abstract_secondary': []}
stop_words_sk = []
stop_words_en = []

def load_data():
    global data
    global stop_words_sk
    global stop_words_en
    lst = con.execute('SELECT * FROM abstract;').fetchall()
    data['id'] = [x[0] for x in lst]
    data['abstract_primary'] = [x[1] for x in lst]
    data['abstract_secondary'] = [x[2] for x in lst]
    with open('./stopwords-sk.txt', 'r', encoding='utf-8') as f:
        stop_words_sk = set([x.strip() for x in f.readlines()])
    stop_words_en = set(stopwords.words())

def clean_and_normalize():
    
    def clean_sk(text):
        sentences = sent_tokenize(text)
        cleaned_sentences = []
        for sentence in sentences:
            sentence = re.sub(r'https?://', '', sentence)
            sentence = sentence.translate(str.maketrans('', '', string.punctuation))
            
            name_list = names.words()
            sentence = re.sub(r'\b[A-Z][a-z]*\b', lambda x: '' if x.group() in name_list else x.group(), sentence)

            tokens = word_tokenize(sentence)

            tokens = [word for word in tokens if not word in stop_words_sk]

            cleaned_sentence = ' '.join(tokens)
            cleaned_sentences.append(cleaned_sentence)
        return cleaned_sentences
    
    def clean_en(text):
        if not text:
            return []
        sentences = sent_tokenize(text)
        cleaned_sentences = []
        for sentence in sentences:
            sentence = re.sub(r'https?://', '', sentence)
            sentence = sentence.translate(str.maketrans('', '', string.punctuation))
            
            name_list = names.words()
            sentence = re.sub(r'\b[A-Z][a-z]*\b', lambda x: '' if x.group() in name_list else x.group(), sentence)

            tokens = word_tokenize(sentence)

            tokens = [word for word in tokens if not word in stop_words_en]

            cleaned_sentence = ' '.join(tokens)
            cleaned_sentences.append(cleaned_sentence)
        return cleaned_sentences
    
    global data
    global stop_words_sk
    global stop_words_en
    edit = []
    for i in range(len(data['id'])):
        edit.append((data['id'][i], '. '.join(clean_sk(data['abstract_primary'][i])), '. '.join(clean_en(data['abstract_secondary'][i]))))
    
    #add lemmization
    
    con.execute("DELETE FROM filtered;")
    con.executemany("INSERT INTO filtered VALUES (?,?,?)", edit)
    con.commit()

load_data()
clean_and_normalize()