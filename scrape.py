import json
from playwright.sync_api import sync_playwright, TimeoutError as PlaywrightTO
import sqlite3

con = sqlite3.connect('./data.sqlite3')

ids = set()

p = sync_playwright().start()
browser = p.chromium.launch(headless=True)
context = browser.new_context()

page = context.new_page()
page.goto('https://opac.crzp.sk/?fn=ResultFormChildAEDKQ&seo=CRZP-Zoznam-z%C3%A1znamov', timeout=0)
page.wait_for_selector("//div[@class='top-buffer']//a")

links = page.locator("//div[@class='top-buffer']//a")
links.nth(0).click()
page.wait_for_selector("//div[@class='well well-sm']")

data = {'id': [], 'abstract_primary': [], 'abstract_secondary': [], 'keyword': []}

def scrape(num):
    global data
    for i in range(num):
        success = False
        while success == False:
            try:
                curr = page.query_selector(".well-sm.hidden-xs.hidden-sm").inner_text()
                next = page.locator('a:has-text("Nasledujúci záznam")')
                next.click()
                js_expr = f"document.querySelector('.well-sm.hidden-xs.hidden-sm').innerText !== '{curr}'"
                with open('./progress.txt', 'w') as f:
                    f.write(curr)
                page.wait_for_function(js_expr)
                page.wait_for_load_state('networkidle')
                
                sid = None
                keyword = None
                primary = None
                secondary = None
                
                content = page.locator("//dl[@class='detail-list inline-list no-margin panel-body']")
                if content.count() > 0:
                    if 'slovenčina' in content.nth(0).text_content():
                        pass
                    else:
                        print('not sk')
                        continue
                else:
                    print('no content 1')
                    continue
                
                content = page.locator("//dl[@class='detail-list panel panel-default panel-body']//a")
                if content.count() > 0:
                    url = content.nth(0).text_content()
                    sid_index = url.find("&sid=")
                    if sid_index != -1:
                        sid_index += len("&sid=")
                        sid = url[sid_index:]
                        if sid in ids:
                            print('already scraped')
                            continue
                    else:
                        print('no sid found')
                        continue
                else:
                    print('no content 2')
                    continue
                
                content = page.locator("//div[@class='well well-sm']")
                if content.count() > 0:
                    key = page.locator("//a[@class='btn btn-default text-wrap']")
                    if key.count() > 0:
                        keyword = [key.nth(x).text_content().strip() for x in range(key.count())]
                    else:
                        print('no keywords')
                        continue
                    primary = content.nth(0).text_content()
                    if content.count() > 1:
                        secondary = content.nth(1).text_content()
                else:
                    print('no abstract')
                    continue
                
                data['id'].append(sid)
                data['keyword'].append(keyword)
                data['abstract_primary'].append(primary)
                data['abstract_secondary'].append(secondary)
                success = True
            except PlaywrightTO:
                success = False
            except Exception as e:
                continue
        print(i)
        if i % 10 == 9:
            if data['id'] != []:
                write_DB()

def write_DB():
    global data
    global ids
    load_url()
    for i in range(len(data['id'])):
        try:
            if data['id'][i] in ids:
                continue
            con.execute("INSERT INTO abstract VALUES (?,?,?)", (data['id'][i], data['abstract_primary'][i], data['abstract_secondary'][i]))
            for key in data['keyword'][i]:
                con.execute("INSERT INTO keyword VALUES (?,?)", (data['id'][i], key))
        except:
            continue
    data = {'id': [], 'abstract_primary': [], 'abstract_secondary': [], 'keyword': []}
    con.commit()

def load_url():
    global ids
    fetch = con.execute("SELECT id FROM abstract;").fetchall()
    for x in fetch:
        ids.add(x[0])

load_url()
scrape(300000)
